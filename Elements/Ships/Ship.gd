tool
extends KinematicBody2D

#these are baseline 
export (float) var MaxSpeed = 100
export (float) var Acceleration = 100
export (float) var Handling = 100
export (float) var Breaking = 100
export (float) var Bounce = 50
export (float) var MaxHealth = 100


export (String,"all","stagger") var GunType = "all"
export (float) var FireRate = 1

var Health:float setget setHealth,getHealth #= float(MaxHealth)

var weaponsHot = 0

func _ready():
	Health = float(MaxHealth)
	$Sprite.material.set_shader_param("ShipTexture",$Sprite.texture)
	$Sprite.material.set_shader_param("Health",min(Health,MaxHealth)/MaxHealth)
	position = Vector2()

func setHealth(newHP):
	Health = newHP
	var temp = min(Health,MaxHealth)
	$Sprite.material.set_shader_param("Health",temp/MaxHealth)
	
func getHealth():
	return Health
	
func setColor(hue):
	$Sprite.material.set_shader_param("Hue",hue)

func _process(delta):
	if get_parent().has_node("Controller"):
		$Thrusters.visible = get_parent().get_node("Controller").thrust

func _physics_process(delta):
	shotCharge = shotCharge + delta
	
func getProperties():
	return {
		"MaxSpeed" : MaxSpeed /100.0,
		"Acceleration" : Acceleration /100.0,
		"Handling" : Handling / 100.0,
		"Breaking" : Breaking /100.0,
		"Bounce": Bounce /100.0,
		"MaxHealth":MaxHealth,
		"Health":Health
	}

var gunIndex = 0

var shotCharge = 0.0
func shoot():
	if weaponsHot:
#	shotCharge = shotCharge + delta
		if shotCharge > FireRate:
			shotCharge = 0
			if GunType == "all":
				for gun in $Guns.get_children():
					gun.shoot(self)
			if GunType == "stagger":
				
				$Guns.get_children()[gunIndex].shoot(self)
				gunIndex = (gunIndex + 1) % $Guns.get_child_count()
		
func takeDamage(dmg,source):
#	print("damage taken!" , dmg, "/",MaxHealth)
	setHealth(Health - dmg)
	if Health <= 0:
		killPlayer(source.get_parent())
		explode()
#		print("player died!")
		
	
func killPlayer(source):
	get_parent().killPlayer(source)

var team setget , getTeam
func getTeam():
	return get_parent().Team
	
var Explotion = preload("res://Elements/Explotion.tscn")
func explode():
	var explotion = Explotion.instance()
	explotion.hue = getTeam()/ 6.0
	explotion.size =explotion.size * 4
	explotion.duration = 1
	explotion.brightness = 8
	explotion.scale = explotion.scale * 4
	explotion.global_position = global_position
	LevelManager.currentLevel.add_child(explotion)

	