extends Node




func _ready():
	var a = Vector2.UP
	var b = a
	b.x = 100
	print(b)
	

func getControls():
	var output = {}
	var p = "p" + String(get_parent().PlayerNumber)
	output.up =    Input.is_action_pressed(p + "up")
	output.down =  Input.is_action_pressed(p + "down")
	output.left =  Input.is_action_pressed(p + "left")
	output.right = Input.is_action_pressed(p + "right")
	output.a = Input.is_action_pressed(p + "a")
	output.b = Input.is_action_pressed(p + "b")
	output.c = Input.is_action_pressed(p + "c")

	output.y = int(output.up) - int(output.down)
	output.x = int(output.right) - int(output.left)
	
	return output
	
var mults = {
	"accel" : 10,
	"break": 10,
	"handle":5,
	"max":5
	}

var thrust:bool = false

var controls
var controlsOld
func _physics_process(delta):
	var ship: KinematicBody2D = get_parent().get_node("myShip")
	if ship != null:
		var speed = get_parent().speed
		thrust=false
		controlsOld = controls
		controls = getControls()
		
		var shipProps = ship.getProperties()
		#print( shipProps )
		if controls.a:# and not controlsOld.a:
			get_parent().get_node("myShip").shoot()
		#turn
		if controls.up:
			thrust=true
			speed += Vector2(0,1).rotated(ship.rotation) * delta * shipProps.Acceleration * mults.accel
			speed = speed.clamped(shipProps.MaxSpeed* mults.max)
		elif controls.down:
			var breakingForce: Vector2 = Vector2(1,0).rotated(speed.angle()) * shipProps.Breaking * mults.break * delta
			if speed.length_squared() < breakingForce.length():
				speed = Vector2()
			else:
				speed = speed - breakingForce
	
		if controls.x:
			var angle = controls.x * delta * shipProps.Handling * mults.handle
			if controls.up:
				angle = angle * 0.25
			ship.rotate(angle ) 
			
		
		var collide = ship.move_and_collide(speed * delta * 60)
		if collide:
			# print("collided!", collide.normal)
			speed = speed.bounce(collide.normal) * shipProps.Bounce
	
		get_parent().speed = speed
