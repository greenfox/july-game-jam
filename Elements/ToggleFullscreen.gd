extends Node

func _ready():
	OS.window_fullscreen = true
	randomize()
	pass
	
func _input(event):
	if event.is_action_pressed("fullScreen"):
		OS.window_fullscreen = !OS.window_fullscreen

func _process(delta):
	if Input.is_action_just_pressed("ESC"):
		if OS.get_name() == "HTML5":
			print("not quiting because HTML5")
#			JavaScript.eval(JSToggleFullscreen)
		else:
			print("quiting")
			get_tree().quit()
			

func _physics_process(delta):
	pass
	
var currentLevel = null
func setCurrentLevel(level):
	currentLevel = level
	
func getCurrentLevel():
	return currentLevel
	
	