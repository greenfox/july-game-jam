extends Node

var LaserGun = {
#	"scene" : preload("res://Elements/ShipParts/Guns/LaserGun.tscn"),
	"name" : "LaserGun",
	"bullet": preload("res://Elements/ShipParts/Bullets/Laser.tscn")
}
var LobGun = {
	"name":"LobGun",
	"bullet": preload("res://Elements/ShipParts/Bullets/Lob.tscn")
}
var MissileLauncher = {
	"name":"MissileLauncher",
	"bullet": preload("res://Elements/ShipParts/Bullets/Missile.tscn")
}
var Shotgun = {
	"name":"Shotgun",
	"bullet": preload("res://Elements/ShipParts/Bullets/Shotgun.tscn")
}


var inventory = ["LaserGun","LobGun","MissileLauncher","Shotgun"]
var list = [LaserGun,LobGun,MissileLauncher]