extends Node2D

func _ready():
	if $gameType == null:
		var game = load("res://Levels/GameTypes/Deathmatch.tscn").instance()
		game.name = "gameType"
		add_child(game)
	LevelManager.setLevel(self)
	