extends Node

var firstLevel = null

var currentLevel:Node2D = null
func setLevel(level:Node2D):
	currentLevel = level
	
func getLevel() -> Node2D:
	return currentLevel

var SplashScreen = preload("res://Levels/Splash.tscn")
var Levels = [
	preload("res://Levels/Level3.tscn"),
	preload("res://Levels/Level2.tscn"),
	preload("res://Levels/Level1.tscn")
]

var GameTypes = [preload("res://Levels/GameTypes/Deathmatch.tscn")]

func _process(delta):
	pass

func reset():
	var root = currentLevel.get_parent()
	currentLevel.queue_free()
	root.add_child(SplashScreen.instance())

func _physics_process(delta):
	if Input.is_action_just_pressed("debug"):
		breakpoint

func switchLevels(players):
	var root = currentLevel.get_parent()
	var newLevel:Node2D = Levels[randi() % Levels.size()].instance()
	var newGame:Node2D = GameTypes[randi() % GameTypes.size()].instance()
	newGame.name = "gameType"
	newLevel.add_child(newGame)
	newGame.loadPlayers(players)
	currentLevel.queue_free()
	root.add_child(newLevel)

var Explotion = preload("res://Elements/Explotion.tscn")
func spawnExplotion(location,hue=0): #this definatly does not belong here!, but fuck it!
	var myExpl = Explotion.instance()
	myExpl.position = location
	
	pass